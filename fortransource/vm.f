C234567890121345678901234567890123456789012345678901234567890123456789012
C        PROGRAM "VECTORIAL MODEL" -  1 MARCH 1988 VERSION
C
C*************************************************************************
C                  WRITTEN BY M. C. FESTOU
C (Minor modifications were made by MF on 8 Nov. 1988 on the Tempe version)
C Some unused variables removed on 6 June 1995 in Baltimore when adapting
C the code to run on a unix machine. A little bit of additional trimming
C on 27 June 1995. New printing formats.
C Few printing format changes made on 27 Sept. 96.
C Formula on the collision radius added on 1 April 1997.
C Modification in sub GAUSS (NP and coeff.) and in sub APP on 22-24/4/1997.
C*************************************************************************
C   THIS PROGRAM COMPUTES THE DENSITY DISTRIBUTION OF THE DISSOCIATION
C   PRODUCTS OF PARENT MOLECULES IN COMET COMAE (FIRST DISSOCIATION ONLY)
C   TAKING INTO ACCOUNT THE VECTORIAL COMPOSITION OF THE VELOCITY VECTORS
C   OF BOTH THE PARENT AND THE DAUGHTER PRODUCTS.
C*************************************************************************
C   THE EMISSION OF THE PARENT MOLECULES IS ASSUMED TO BE ISOTROPIC.
C   THE CODE CAN HANDLE A TIME DEPENDENT SOURCE FUNCTION (step function only)
C*************************************************************************
C

        DIMENSION DENS(150,30),DENR(150),X(150),PCC(200),
     +  TH(200),P(200),PCO(200),XCOORD(150),
     +  QP(31),QN(31),TOU(31),RES(200)
        CHARACTER*72 COMET,COM,RADICAL,RAD
        COMMON/DOR/DENR
        COMMON/DCOORD/X,XCOORD
        COMMON/DUR/TAUPT,TAUPD,TAUGT
        COMMON/VIT/VPAR,VDG
        COMMON/TAB/AA,NF,EPSI,JMAX,DALFA
        COMMON/DEST/DESTP,DESTR
        COMMON/C1/GEXC0,QP,QN,TOU,NSTEPS,RHELIO,RH2,DELTA
        !OPEN(UNIT=35,STATUS='OLD',FILE='fparam.dat')
        !Lauren Lyons - 11/12 - Start
        CHARACTER(len=32) :: inputfile
        CALL getarg(1,inputfile)
        !Check that this isnt empty first
        OPEN(UNIT=35,STATUS='OLD',FILE=inputfile)
        !Lauren Lyons - 11/12 - End
        PI=4.*ATAN(1.)

C
C*************************************************************************
C                      COMET UNDER STUDY
C                      =================
C
        READ (35,'(A)') COM
        DO I=36,1,-1
        IF(COM(I:I).NE.' ') GO TO 690
        END DO
 690    WRITE(6,'(A,A,A)') '$Comet under study : ',COM(1:I),
     +  ' .Hit return key or enter new comet name :'
        READ(5,'(A)') COMET
        IF(COMET(1:1).EQ.' ') COMET=COM
        WRITE(16,'(1X,A)') COMET

C
C*************************************************************************
C          HELIOCENTRIC AND GEOCENTRIC DISTANCE OF COMET
C          =============================================
C
        READ(35,*) HELIO,GEO
  702   WRITE (6,'(/,A,A,2F8.3)')' Heliocentric and Geocentric ',
     +  'distances (AU):',HELIO,GEO
        WRITE(6,'(A)') '$Hit return or enter new values : '
        READ(5,'(A)') COM
        IF(COM(1:1).EQ.' ') THEN
        RHL=HELIO
        DGO=GEO
        ELSE
        READ(COM,*,ERR=702) RHL,DGO
        END IF

C
C*************************************************************************
C                  SOURCE OF PARENT MOLECULES
C                  ==========================
C
C EACH DISSOCIATION OF A PARENT MOLECULE PRODUCES ONE RADICAL.
C
C GAS PRODUCTION FUNCTION : N STEPS OF LENGTH DELTAT (days) DURING WHICH
C THE GAS PRODUCTION RATE IS CONSTANT.
C
        READ(35,*) NST
 703    WRITE (6,'(/,A,A,I2)') ' Number of steps in gas production',
     +  ' function. Default is ',NST
        WRITE(6,'(A)')'$Hit return or enter new value :'
 122    READ(5,'(A)') COM
        IF(NSTEPS.GT.20) GO TO 120
        GO TO 121
 120    WRITE (6,'(A,A)')'$NSTEPS must be smaller than 20.',
     +  ' Re-enter NSTEPS : '
        GO TO 122
 121    IF(COM(1:1).EQ.' ') THEN
        NSTEPS=NST
        ELSE
        READ(COM,*,ERR=703) NSTEPS
        END IF
704     WRITE(6,'(/,A,A)')' Enter the gas prod. rate, i.e., Q',
     +  '(mol. per sec) vs beginning time of each step (days) :'
        DO I=1,NSTEPS
          READ(35,*)QPAR,TBE
          WRITE(6,'(A,I2,A,E10.2,A,F6.2)')' STEP',I,' Default is QP= ',
     +    QPAR,' TOU= ',TBE
          WRITE(6,50) '$Hit return or enter new values : Step',I,'/',
     +    NSTEPS,' : '
50        FORMAT(A,I3,A,I2,A)
          READ(5,'(A)') COM
          IF(COM(1:1).EQ.' ') THEN
          QP(I)=QPAR
          TOU(I)=TBE
          ELSE
          READ(COM,*,ERR=704) QP(I),TOU(I)
          END IF
        END DO
        DO I=NSTEPS+1,20
          READ(35,*)QP(I),TOU(I)
        END DO

C
C*************************************************************************
C              PARENT MOLECULE PARAMETERS
C              ==========================
C
C TAUPD IS THE DISSOCIATIVE LIFETIME; TAUPT IS THE TOTAL LIFETIME; TAUPD
C IS LARGER THAN TAUPT. ALL LIFETIMES ARE DEFINED HERE FOR ONE A.U.; THEY
C ARE SUBSEQUENTLY ADJUSTED FOR THE APPROPRIATE HELIOCENTRIC DISTANCE.
C
        READ(35,*) VPA
 705    WRITE (6,'(/,A,A,f8.3,A)')' Parent velocity. Default value',
     +  ' is :',VPA,'(km/s)'
        WRITE(6,'(A)')'$Hit return or enter new value : '
        READ(5,'(A)') COM
        IF(COM(1:1).EQ.' ') THEN
        VPAR=VPA
        ELSE
        READ(COM,*,ERR=705) VPAR
        END IF
        READ(35,*) TOTLIF
 706    WRITE(6,'(/,A,A,F8.0)') ' The total lifetime (s) of the parent',
     +  ' molecules at 1 AU is : ',TOTLIF
        WRITE(6,'(A)')'$Hit return or enter new value : '
        READ(5,'(A)') COM
        IF(COM(1:1).EQ.' ') THEN
        TAUPT1=TOTLIF
        ELSE
        READ(COM,*,ERR=706) TAUPT1
        END IF
        READ(35,*) TOTDISS
 707    WRITE(6,'(/,A,A,F8.0)') ' The dissociative lifetime (s) of',
     +  ' the parent molecules at 1 AU is : ',TOTDISS
        WRITE(6,'(A)')'$Hit return or enter new value : '
        READ(5,'(A)') COM
        IF(COM(1:1).EQ.' ') THEN
        TAUPD1=TOTDISS
        ELSE
        READ(COM,*,ERR=707) TAUPD1
        END IF
        IF(TAUPD1.GE.TAUPT1) GO TO 201
        IF(TAUPD1.LT.TAUPT1) WRITE(6,'(A)') '$RE-ENTER TAUPD1 : '
        GO TO 707
 201    READ(35,*) DESP
 708    WRITE (6,'(/,A,A,F7.2)') ' The destruction level of the parent',
     +  ' molecules is : ',DESP
        WRITE(6,'(A)')'$Hit return or enter 0 < Destp < 100 : '
        READ(5,'(A)') COM
        IF(COM(1:1).EQ.' ') THEN
        DESTP=DESP
        ELSE
        READ(COM,*,ERR=708) DESTP
        END IF

C*************************************************************************
C                 DAUGHTER SPECIES PARAMETERS
C                 ===========================
C
        READ (35,'(A)') RAD
        DO I=36,1,-1
            IF(RAD(I:I).NE.' ') GO TO 691
            END DO
  691   WRITE (6,'(/,A,A,A)') '$Radical whose density distribution is',
     +  ' computed : ',RAD(1:I)
        WRITE(6,'(A)')'$Hit return or enter new radical name : '
        READ(5,'(A)') COM
        IF(COM(1:1).EQ.' ') THEN
        RADICAL=RAD
        ELSE
        RADICAL=COM
        END IF
        WRITE(16,'(1X,A)') RADICAL
        READ(35,*) GEX
 709    WRITE (6,'(/,A,A,E10.3)') ' Excitation Rate (s-1) at 1 AU of ',
     +  'observed transition is : ',GEX
        WRITE(6,'(A)')'$Hit return or enter new value : '
        READ(5,'(A)') COM
        IF(COM(1:1).EQ.' ') THEN
        GEXC0=GEX
        ELSE
        READ(COM,*,ERR=709) GEXC0
        END IF
C
C IN THIS VERSION, UNLESS ONE CONSIDERS THE RADICAL AS A SECONDARY SOURCE
C OF RADICALS, ONE MUST TAKE INTO CONSIDERATION THE TOTAL RADICAL LIFETIME
C
        READ(35,*) VELRAD
  710   WRITE (6,'(/,A,A,F7.3,A)')' Radical velocity. Default value is',
     +  ' : ',VELRAD,' (km/s)'
        WRITE(6,'(A)')'$Hit return or enter new velocity : '
        READ(5,'(A)') COM
        IF(COM(1:1).EQ.' ') THEN
        VDG=VELRAD
        ELSE
        READ(COM,*,ERR=710) VDG
        END IF
        READ(35,*)TRAD
        WRITE (6,'(/,A,A,f8.0)')' Total lifetime of the radicals at ',
     +  '1 AU is : ',TRAD
  711   WRITE(6,'(A)')'$Hit return or enter new value : '
        READ(5,'(A)') COM
        IF(COM(1:1).EQ.' ') THEN
        TAUGT1=TRAD
        ELSE
        READ(COM,*,ERR=711) TAUGT1
        END IF
        READ(35,*) DESR
  712   WRITE (6,'(/,A,A,F7.2)')' The destruction level of the ',
     +  'radicals is :',DESR
        WRITE(6,'(A,F7.2)')'$Hit return or enter 0 < DESTR < 100 : '
        READ(5,'(A)') COM
        IF(COM(1:1).EQ.' ') THEN
        DESTR=DESR
        ELSE
        READ(COM,*,ERR=712) DESTR
        END IF
        READ(35,*) WID,WLENG
 713    WRITE(6,'(/,a)')' Slit dimensions:'
        write(6,'(a,f5.1,a)')' Perpendicularly to the sun-comet line: ',
     +        WID,' arc sec'
        write(6,'(a,f5.1,a)')' Along the sun-comet line: ',WLENG,
     +        ' arc sec'
        WRITE(6,'(A)')'$Hit return or enter new values : '
        READ(5,'(A)') COM
        IF(COM(1:1).EQ.' ') THEN
        XW=WID
        XL=WLENG
        ELSE
        READ(COM,*,ERR=713) wid,wleng
        XW=WID
        XL=WLENG
        END IF

C*************************************************************************
C WRITE PARAMETERS IN DATA FILE fparam
        REWIND 35
           WRITE(35,'(A)') COMET
           WRITE(35,*) RHL,DGO
           WRITE(35,*)NSTEPS
              DO J=1,20
              WRITE(35,*) QP(J),TOU(J)
              END DO
           WRITE(35,*)VPAR
           WRITE(35,*)TAUPT1
           WRITE(35,*)TAUPD1
           WRITE(35,*)DESTP
           WRITE(35,'(A)')RADICAL
           WRITE(35,*)GEXC0
           WRITE(35,*)VDG
           WRITE(35,*)TAUGT1
           WRITE(35,*)DESTR
           WRITE(35,*)XW,XL
C DIMENSION OF TABLE DENS(I,J)
        NF=26
        AA=150.
        VPAR=VPAR*1.E5
        VDG=VDG*1.E5

C************************************************************************* 
C
C CALCULATE THE COLLISION SPHERE RADIUS
c
        SIGMA=1.75E-08**2   !cm**2
        utherm=(0.25*VPAR)  ! vtherm=.25 outflow velocity
        rcoll=sigma*qp(1)*utherm/vpar/vpar
        WRITE(16,'(a,2e10.2,a,/,/,/)')
     +     ' Prod. rate, collision sphere radius is: ',qp(1),rcoll,' cm'
C*************************************************************************

C
C                   CALCULATION OF THE VOLUME DENSITY
C                   *********************************
C
         RHELIO=RHL
         RH2=RHL**2
         TAUPT=TAUPT1*RH2
         TAUPD=TAUPD1*RH2
         TAUGT=TAUGT1*RH2
         GEXC=GEXC0/RH2
         DELTA=DGO
         CALL SETUP                      !INITIALIZATION OF PROGRAM
         WRITE(16,'(A,e8.2,A,/)') ' Collision sphere radius is: ',
     +	 rcoll, ' cm'
         CALL SDENT(DENS)                !ELEMENTARY 2D DENSITY TABLE
         CALL SYM(DENS,DENR)             !COMPUTE DENSITY PROFILE
         CALL VERIF                      !DENSITY CALCULATION CHECK

C*************************************************************************
C
C                 CALCULATION OF THE COLUMN DENSITIES
C                 ***************************************
C
       EXPOO=ALOG10(0.85*XCOORD(1)/XCOORD(JMAX))   !P(MAX)=85% OF DIM
       DEXPO=0.05           !DECREASE DEXPO IF ACCURACY IS NEEDED IN APP
       JVDMAX=IFIX(EXPOO/DEXPO)
       WRITE (16,276)
 276   FORMAT(/,' CHECK ON COLUMN DENSITY CALCULATION ACCURACY :',
     + ' "S1/S2" should not differ from 100 by more than '/,
     + ' 1 unit except near the center or near the edge of the coma')
       WRITE(16,'(A,A)')' **  P(cm)   SG1(trapeziums)  SG2(Gauss)',
     +  '    nseg    G1      G2      G3      S1/S2'
        DO 842 JVD=1,JVDMAX
        EXPO=(JVD-1.)*DEXPO+0.0001
        P(JVD)=(10.**(EXPO+ALOG10(XCOORD(JMAX))))*1.E5   !P IN CENTIMETERS
        CALL SLINS(P(JVD),TH(JVD),JVD)    !INTEGRATION ALONG LINES OF SIGHT
        JVDM=JVD
        PCC(JVD)=P(JVD)/1.E5                            !PCC IN KILOMETERS
 842    CONTINUE
        WRITE(16,5430)
        WRITE(16,638)
        WRITE(16,639)(PCC(JL),TH(JL),JL=1,JVDM)    !PRINT COLUMN DENSITIES
        WRITE(16,444)

C*************************************************************************
C
C       AVERAGE BRIGHTNESS OVER THE APERTURE
C       ************************************
C
c SLIT 1: 1.3 by 3.6 arc sec:
       xw=1.3                             !HST FOS SLIT
       xl=3.6
       write(16,'(a)')' FOS slit: 1.3 by 3.6 arc sec'
       call SLIT(xw,xl,delta,p,pco,th,res,jvdm,jv,gexc)

c SLIT 2: 9.1 by 10.3 arc sec:
       xw=9.1                             !IUE central part of the slit
       xl=10.3
       write(16,'(a)')' IUE slit: 9.1 by 10.3 arc sec'
       call SLIT(xw,xl,delta,p,pco,th,res,jvdm,jv,gexc)

c SLIT 3: 9.1 by 15.3 arc sec:
       xw=9.1                             !IUE rectangular part of the slit 
       xl=15.3
       write(16,'(a)')' IUE slit: 9.1 by 15.3 arc sec'
       call SLIT(xw,xl,delta,p,pco,th,res,jvdm,jv,gexc)

c SLIT 4: customized slit
       xw=wid
       xl=wleng
       write(16,'(a,f5.1,a,f5.1,a)')' Customized FOV: ',wid,' by ',
     +      wleng,'  arc sec'
       call SLIT(xw,xl,delta,p,pco,th,res,jvdm,jv,gexc)

C*************************************************************************
 232     FORMAT(A,2F9.3)
 233     FORMAT(A,E10.2,A,F7.2)
 235     FORMAT(A,E10.3,A,E10.3,A,F7.2)
 268     FORMAT(/,A,F7.2,2A,F7.2,A)
 444     FORMAT(/)
 638     FORMAT(/,4('   P(km)       N(cm-2)  ')/)
 639     FORMAT(4(X,E10.3,X,1PE10.2,2X))
 5430    FORMAT(1H1)
        END

C        **********************************************************
C        *                                                        *
           SUBROUTINE SLIT(xw,xl,delta,p,pco,th,res,jvdm,jv,gexc)
C        *                                                        *
C        **********************************************************
       COMMON/TAB/AA,NF,EPSI,JMAX,DALFA
       dimension p(200),pco(200),th(200),res(200)
       pi=4.*atan(1.)
       XW=XW*DELTA*1.5E13*PI/3600./180.  !on perpendicular to radius vector
       XL=XL*DELTA*1.5E13*PI/3600./180.  !along radius vector
       dgg=0.5*sqrt(xl**2+xw**2)         !Half diagonal of the slit
       if(dgg.gt.p(jvdm)) stop ' The slit is too large'
       JV=0
       DO ISLIT=1,JVDM,3
         pm=sqrt((p(islit)+xl/2.)**2+xw**2/4.)
         if(pm.gt.p(jvdm)) go to 1
         JV=JV+1
         PCO(JV)=P(ISLIT)
       END DO
c      write(16,*)xw,xl,pm,p(jvdm)
c      write(16,*)(pco(i),i=1,jv)
 1     CALL APP(TH,PCO,P,RES,JVDM,JV,XL,XW,GEXC)
       return
       end

C              *************************************************
C              *                                               *
                            SUBROUTINE SETUP
C              *                                               *
C              *************************************************
C
C
       DIMENSION XCOORD(150),NBB(11),X(150),QP(31),QN(31)
       DIMENSION TOU(31)
       COMMON/DCOORD/X,XCOORD
       COMMON/DUR/TAUPT,TAUPD,TAUGT
       COMMON/VIT/VPAR,VDG
       COMMON/TAB/AA,NF,EPSI,JMAX,DALFA
       COMMON/DEST/DESTP,DESTR
       COMMON/SPHER/DIM,RCOMA,TPERM
       COMMON/C1/GEXC0,QP,QN,TOU,NSTEPS,RHELIO,RH2,DELTA
       COMMON/C2/TV,COEFF
C
C SET NETWORK FOR DISSOCIATING THE PARENTS:
C THE PARENT MOLECULES ARE DESTROYED UP TO DISTANCE RCOMA. THE VALUE OF RCOMA
C MEASURES THE FRACTION OF THE TOTAL GASEOUS OUTPUT OF THE NUCLEUS WHICH IS
C DISSOCIATED (DESTP %) AND WHICH IS TAKEN INTO ACCOUNT IN THIS PROGRAM.
C
       DATA NBB/10,1,2,3,5,8,12,18,25,40,60/  !DETERMINES THE CALCULATIONS
!                                              ACCURACY
      PI=4.*ATAN(1.)
C------------------------------------------------------------------------
C DETERMINE COMA SIZE :
       CDIM=-ALOG(1.-DESTR/100.)
       GDIM=-ALOG(1.-DESTP/100.)
C RCOMA IS LIMITED EITHER BY "TOU(1)" OR BY "DESTP", WHICHEVER IS THE
C SMALLEST.
       RCOMA=GDIM*VPAR*TAUPT
       VXZ=VPAR*TOU(1)*86400.              !TOU(1) IN DAYS
       IF(VXZ.LT.RCOMA) RCOMA=VXZ
C DIM1 CORRESPONDS TO THE PERMANENT FLOW REGIM, DIM2 TO AN OUTBURST SITUATION
       DIM1=RCOMA+(VDG+VPAR)*CDIM*TAUGT
       DIM2=(VDG+VPAR)*TOU(1)*86400.
       DIM=AMIN1(DIM1,DIM2)
       EPSI=PI
       IF(VDG.LE.VPAR) EPSI=ASIN(VDG/VPAR)
       TPERM=(RCOMA/VPAR+(DIM1-RCOMA)/(VDG+VPAR))/86400.
C -------------------------------------------------------------------
C PRINT CALCULATION PARAMETERS :
       WRITE(16,381) GEXC0,RHELIO,DELTA
 381   FORMAT(1X,'EXCITATION RATE AT 1 AU IS ',1pE10.3,' PHOT/MOL/S',
     + 3X,' RHELIO=',0pF7.3,'A.U.  DELTA =',F7.3,'AU')
       WRITE(16,100) DESTP
       WRITE(16,843) DESTR
 100   FORMAT(1X,'THE DESTRUCTION LEVEL OF THE PARENT MOLECULES IS:',
     + ' DESTP=',F5.1,' %')
 843   FORMAT(1X,'THE DESTRUCTION LEVEL OF THE RADICALS IS:',
     + '         DESTR=',F5.1)
       IF(TOU(1).GE.TPERM) WRITE(16,76) TPERM
 76    FORMAT(1X,'PERMANENT FLOW REGIM : TPERM=', F6.2,' DAYS')
       IF(TOU(1).LT.TPERM) WRITE(16,77) TPERM, TOU(1)
 77    FORMAT(1X,'OUTBURST SITUATION : TPERM=',F6.2,' DAYS; TOU(1)=',
     + F6.2,' DAYS BEFORE PRESENT')
       TOU(NSTEPS+1)=0.
       QP(NSTEPS+1)=QP(NSTEPS)
       DO I=1,NSTEPS
        WRITE (16,194) QP(I),TOU(I),TOU(I+1)
        TOU(I)=TOU(I)*86400.
       END DO
 194   FORMAT (1X,' THE GAS PRODUCTION IS ',1pE9.2,' BETWEEN ',0pF6.2,
     + ' DAYS AND',F6.2,' DAYS')
       TPP=TAUPD/RH2
       TPPT=TAUPT/RH2
       TGG=TAUGT/RH2
       VVP=VPAR/1.E5
       VVD=VDG/1.E5
       RCCC=RCOMA/1.E5
       DDIM=DIM/1.E5
       WRITE(16,103) VVP,VVD,TPP,TPPT,TGG,RCCC,DDIM,DIM1,DIM2
 103   FORMAT(1X,'VPAR=',F6.3,' KM/S;  VDG=',F6.3,' KM/S;',
     +' TPDISS=',F8.0,'S;  TPTOT=',F8.0,'S  TGTOT=',
     +F8.0,'S;  RCOMA=',1PE9.3,'(KM); DIM=',1PE9.3,' KM; Perm=',1PE9.3,
     +' ; Burst=',1PE9.3)
       TV=TAUPT*VPAR
       DO I=1,NSTEPS+1
       QN(I)=QP(I)/VPAR
       END DO
       COEFF=1./(4*PI)/TAUPD
C -------------------------------------------------------------------------
C SET DENSITY TABLE DENS(I,J):
C INDEX I REFERS TO THE DISTANCE TO THE NUCLEUS; THE INDEX J REFERS 
C TO THE ANGLE FROM THE EJECTION DIRECTION. AA IS SMALLER OR EQUAL TO 
C MAX(I) AND NF IS SMALLER TO OR EQUAL TO MAX(J). 
C DAUGHTER PRODUCT DENSITY NETWORK: 
C TABLES X OR XCOORD. THIS NETWORK IS OF THE TYPE: L*RES*(1+2+4+10+ ...)
C =DIM. NBB(end)*RES IS THE SIZE OF THE LARGEST DENSITY BOX; L DEPENDS ON 
C THE SIZE OF TABLE DENS(I,J).
C THE GRADIENT ALONG ALFA IS SMALLER THAN THE GRADIENT ALONG R. THE 
C
       N2=0
       DO 11 KMN=2,NBB(1)+1
           N2=N2+NBB(KMN)
 11    CONTINUE
       A=AA/NBB(1)
       L=IFIX(A)
       IF(L.LE.1) GO TO 1
       RES=DIM/(N2*L)
       JMAX=NBB(1)*L
       AD=0.
       DO 9 I=1,NBB(1)
           DRES=RES*NBB(I+1)
       DO J=1,L
           JJ=JMAX+1-(I-1)*L-J
           X(JJ)=AD+(J-.5)*DRES
           XCOORD(JJ)=X(JJ)/1.E+5
       END DO
 9     AD=AD+L*DRES
       DALFA=EPSI/NF
       WRITE(16,385) L,JMAX,NF,DALFA
       WRITE(16,386) res/1.e5
       RETURN
 385   FORMAT(1X,' L=',I2,';   JMAX=',I3,';  NF=',I2,'; DALFA=',F5.3,
     + ' RD')
 386   FORMAT(' Spatial resolution near the nucleus is: ',f5.0,' km')
 1     WRITE(16,2)
 2     FORMAT(/'  THE DIMENSION OF TABLE DENS IS UNSUFFICIENT')
       END

C
C                **********************************************
C                *                                            *
                            SUBROUTINE SDENT(DENS)
C                *                                            *
C                **********************************************
C
C THIS SUBROUTINE COMPUTES THE DENSITY IN A HALF-MERIDIAN PLANE CONTAINING 
C THE NUCLEUS.THE ELEMENTARY EJECTION ANGLE IS DALFA
C
       DIMENSION DENS(150,30),X(150),XCOORD(150),RX(12),QP(31),QN(31)
       DIMENSION TOU(31)
       EXTERNAL QT
       COMMON/DCOORD/X,XCOORD
       COMMON/VIT/VPAR,VDG
       COMMON/TAB/AA,NF,EPSI,JMAX,DALFA
       COMMON/SPHER/DIM,RCOMA,TPERM
       COMMON/C1/GEXC0,QP,QN,TOU,NSTEPS,RHELIO,RH2,DELTA
       COMMON/C2/TV,COEFF
       COMMON/DUR/TAUPT,TAUPD,TAUGT
       PI=4.*ATAN(1.)
       DO I=1,JMAX
          DO J=1,NF
             DENS(I,J)=0.
          END DO
       END DO
       VDGG=VDG*VDG
       VPARR=VPAR*VPAR
       TLIMI=8.*TAUGT  !FOLLOW RADICALS UNTIL THEY HAVE BEEN TOTALLY DESTROYED
       RLIM=RCOMA
       RC1=RCOMA/3.
       RC2=2.*RC1
       RD=RCOMA+(DIM-RCOMA)/2.
       R2=RCOMA**2.
C ----------------------------------------------------------------------
C BUILT THE 2D ELEMENTARY DENSITY TABLE
          DO 61 J=1,NF
          ANGLE=DALFA*(J-0.5)
          DO 60 I=1,JMAX
          DIST=X(I)
          XX=DIST*SIN(ANGLE)
          YY=DIST*COS(ANGLE)
          DRLIM=XX/TAN(EPSI-0.0001)
          IF(EPSI.LT.3.14159) RLIM=YY-DRLIM
          NTT=3
          IF(YY.LT.RD.AND.XX.LT.RCOMA) NTT=6
          IF(YY.LT.RCOMA.AND.XX.LT.RC2) NTT=9
          IF(XX.LT.RC1) NTT=12
          DEPSI=(EPSI-ANGLE)/NTT
          P2=XX*XX+(YY-RCOMA)**2
          P22=SQRT(P2)
          Q2=XX*XX+YY*YY
          Q22=SQRT(Q2)
          IF(EPSI.GT.3.14159) DEPSI=1./NTT*ACOS((P2+Q2-R2)/
     +    (2.*P22*Q22))
          RX(NTT)=RLIM
          NTO=NTT-1
            DO ILW=1,NTO
            RX(ILW)=1./COS(ANGLE)*(DIST-COS(ILW*DEPSI)*(XX/COS(
     +      PI/2-ILW*DEPSI-ANGLE)))
            END DO
          IF(I.NE.10) GO TO 17
          DANGLE=ABS(PI/2.-ANGLE)
          IF(DANGLE.LT.0.01) WRITE(16,726) I,J,ANGLE,DANGLE,(RX(KT),
     +    KT=1,NTT)
 726      FORMAT(1X,2I4,2(1PE10.3)/,12(1PE10.3)//)
 17       CONTINUE
          RXO=0.
          NB=7
            DO 66 KX=1,NTT
            DR=(RX(KX)-RXO)/NB
            DO 811 KY=1,NB
            R=(KY-0.5)*DR+RXO
            RP2=XX*XX+(R-YY)**2
            RP22=SQRT(RP2)
            TEMPS=R/VPAR
            IF(YY.LT.R) GAMMA=ATAN(XX/(R-YY))
            IF(YY.EQ.R) GAMMA=PI/2.
            IF(YY.GT.R) GAMMA=PI-ATAN(XX/(YY-R))
            OMEGA=PI-GAMMA
            COTET=COS(OMEGA)
            BQ=COS(GAMMA)
            DQ=VDGG-VPARR*(1.-BQ*BQ)
            DQ=SQRT(DQ)
            VSUP=-VPAR*BQ+DQ
            IF(YY.EQ.R) VSUP=1.E-5
            TVOLS=RP22/VSUP
            IF(TVOLS.GT.TLIMI) GO TO 812
            TTOTS=TEMPS+TVOLS
            FGAMAS=VSUP*VSUP/VDG/(VSUP-VPAR*COTET)*QT(TTOTS)
            EXTIN=EXP(-R/TV)
            AST = TVOLS/TAUGT
            EXTS=0.
            IF (AST.LT.500.)EXTS =EXP(-AST)
            DNS=EXTIN/RP2*EXTS*FGAMAS/VSUP
            DENS(I,J)=DENS(I,J)+DNS*DR
 813        IF(VDG.GE.VPAR) GO TO 812
            VINF=-VPAR*BQ-DQ
            TVOLI=RP22/VINF
            IF(TVOLI.GT.TLIMI) GO TO 812
            TTOTI=TEMPS+TVOLI
            FGAMAI=-VINF*VINF/VDG/(VINF-VPAR*COTET)*QT(TTOTI)
            AST=TVOLI/TAUGT
            EXTI=0.
            IF(AST.LT.500.) EXTI=EXP(-AST)
            DNI=EXTIN/RP2*EXTI*FGAMAI/VINF
            DENS(I,J)=DENS(I,J)+DNI*DR
 812        CONTINUE
 811        CONTINUE
            RXO=RX(KX)
 66         CONTINUE
 70         DENS(I,J)=DENS(I,J)*COEFF*DALFA*DALFA/(4.*PI)
 60         CONTINUE
                 COST=FLOAT(J)/FLOAT(NF)*100.
                 print*,char(27)//' SDENT : ',COST,' %'
                 ! print*,char(27)//'[23;1H',' SDENT : ',COST,' %'
 61         CONTINUE
       DO IH=1,JMAX
               IF(VDG.GT.VPAR) DENS(IH,NF+1)=DENS(IH,NF)
               IF(VDG.LE.VPAR) DENS(IH,NF+1)=0.
       END DO
       RETURN
       END

C
C           *****************************************
C           *                                       *
                        FUNCTION QT(TOUT)
C           *                                       *
C           *****************************************
C
        DIMENSION QP(31),QN(31),TOU(31)
        COMMON/C1/GEXC0,QP,QN,TOU,NSTEPS,RHELIO,RH2,DELTA
C STEP PRODUCTION FUNCTION
        DO 10 I=1,NSTEPS
 10     IF (TOUT.LT.TOU(I).AND.TOUT.GT.TOU(I+1)) QT=QN(I)
        RETURN
        END

C
C           *******************************************
C           *                                         *
                     SUBROUTINE SYM(DENS,DENR)
C           *                                         *
C           *******************************************
C
C  THIS SUBROUTINE IS WRITTEN FOR THE CASE WHERE THE EJECTION CONE
C  IS 4.*PI. THUS, SPHERICAL SYMMETRY IS ASSUMED.
C  DENSITIES ARE LINEARLY INTERPOLATED. THIS CALCULATION COULD
C  BE IMPROVED BY THE USE OF A SPLINE SUBROUTINE (EFFECTIVE ONLY
C  NEAR THE NUCLEUS).
C
       DIMENSION DENS(150,30),DENR(150),X(150),XCOORD(150)
       COMMON/DCOORD/X,XCOORD
       COMMON/TAB/AA,NF,EPSI,JMAX,DALFA
       PI=4.*ATAN(1.)
C THIS CALCULATION IS VALID ONLY FOR AN ISOTROPIC COMA
          DO J=1,JMAX
          DENR(J)=0.
          END DO
              DO I=1,JMAX
              DO J=1,NF
              TETA=DALFA*(J-.5)
              SJO=2.*SIN(TETA)*PI*DENS(I,J)/DALFA
              DENR(I)=DENR(I)+SJO
              END DO
              print *, I, DENR(I)
              END DO
       WRITE(16,17)
 17    FORMAT('  R (km) and DAUGHTER PRODUCT DENSITIES (cm**-3)'/)
       write(16,'(4(2e10.2,2x))')(xcoord(i),denr(i),i=jmax,1,-3)
       RETURN
       END

C
C             *******************************************
C             *                                         *
                       SUBROUTINE SLINS(P,SG,JVD)
C             *                                         *
C             *******************************************
C
C  THIS SUBROUTINE COMPUTES THE INTEGRATED DENSITY ALONG A LINE OF SIGHT
C  WHOSE IMPACT PARAMETER IS P. THE TOTAL NUMBER OF RADICALS INSIDE THE
C  COMA IS CALCULATED IN SUBROUTINE VERIF BY SUM(4.0*PI*DENR(X)*X*X*DX).
C
       EXTERNAL FONCB,GAUSS
       COMMON/DOR/DENR
       COMMON/DCOORD/X,XCOORD
       COMMON/TAB/AA,NF,EPSI,JMAX,DALFA
       COMMON/DATINT/XL,YL
       COMMON/SKI/DRAS,PP
       DIMENSION DENR(150),X(150),XCOORD(150),DRAS(150)
       DIMENSION XL(3),YL(3)
       DOUBLE PRECISION AAA(3),BBB(3),CCC(3),DDD(3)
       PP=P*P
! PUT TABLES XCOORD AND DENR IN ASCENDING ORDER.
       DO KR=1,JMAX
          KL=JMAX+1-KR
          X(KR)=XCOORD(KL)*1.E5            !CENTIMETERS
          DRAS(KR)=DENR(KL)
       END DO
       SG=0.
       IF (X(JMAX).LT.P) RETURN    !IMPACT PARAMETER LARGER THAN COMA
       IF(X(1).GE.P)     RETURN
C*************** FIRST METHOD : TRAPEZIUMS *******************************
C
       DO  IT =1,JMAX              !FIND FIRST VALUE OF X(I) GT P.
          ITT=IT
          IF(X(IT).GT.P) GO TO 1
       END DO
C CONSTITUTE TABLES XL AND YL BEFORE INTERPOLATION
 1     IF(JMAX.LT.3) THEN
            WRITE(16,*)' JMAX IS TOO SMALL, INTERPOLATION IS',
     +      ' NOT POSSIBLE'
            STOP 872
       ELSE
           IF(ITT.EQ.JMAX) IT=IT-1        !CASE : P.GT.X(JMAX-1)
           DO JF=1,3
           XL(JF)=X(IT+JF-2)
           YL(JF)=DRAS(IT+JF-2)
           END DO
       END IF
       CALL SPLINE(XL,YL,AAA,BBB,CCC,DDD,3,IER)
       IF(IER.LT.0) THEN
       WRITE(16,210) IER
210    FORMAT('---trapeziums--> ERROR ',I2,' IN SUBR. SPLINE <----')
       STOP
       END IF
       CALL ISPLIN(XL,YL,AAA,BBB,CCC,DDD,3,P,D2,IER)
26     S=SQRT(X(ITT)**2-PP)
       SG = S*(D2+DRAS(ITT))/2.
       SGG=0.
       MAX = JMAX-1
       DO 4 IW = ITT,MAX            !INTEGRATION WITH TRAPEZIUMS
          S1=SQRT(X(IW)**2-PP)
          S2=SQRT(X(IW+1)**2-PP)
          SGG=(S2-S1)*(DRAS(IW)+DRAS(IW+1))/2.
          SG=SG+SGG
 4     CONTINUE
       SG=SG*2.
       SG1=SG
C
C **************** SECOND METHOD : GAUSS METHOD ****************
C
C INTEGRATION LIMITS ON LINE OF SIGHT
       PL1=0.15*X(JMAX)
       PL2=0.4*X(JMAX)
       NSEG=5
       IF(P.LT.PL2) NSEG=10
       IF(P.LT.PL1) NSEG=25
 25    SINF=0.
       SSUP=SQRT(X(JMAX)**2-PP)
       SINT1=(SSUP-SINF)/NSEG
       SINT2=3.*SINT1
       T1=GAUSS(SINF,SINT1,8,FONCB,IER)
       T2=GAUSS(SINT1,SINT2,8,FONCB,IER)
       T3=GAUSS(SINT2,SSUP,8,FONCB,IER)
       SG2=(T1+T2+T3)*2.
       T11=T1/SG2*2.
       T22=T2/SG2*2.
       T33=T3/SG2*2.
       IF(T11.GE.0.70) THEN
       NSEG=NSEG*1.3
       GO TO 25
       ELSE
       END IF
       ratio=sg1/sg2*100.
       P1=ALOG10(P)
       SG11=ALOG10(SG1)
       SG22=ALOG10(SG2)
       FQ=JVD/5.
       KFQ=FQ
       REAL=FQ-KFQ
       IF(REAL.NE.0.) GO TO 19
       WRITE(16,17) p,sg1,sg2,NSEG,T11,T22,T33,ratio
 17    FORMAT(1X,3(E10.3,4X),I5,3F8.3,3X,F6.1)
 19    SG=SG2
       RETURN
       END
C
C             *********************************************
C             *                                           *
                           FUNCTION FONCB(S)
C             *                                           *
C             *********************************************
C
       DIMENSION XL(3),YL(3),DENR(150),X(150),XCOORD(150),DRAS(150)
       DOUBLE PRECISION AAA(3),BBB(3),CCC(3),DDD(3)
       COMMON/DOR/DENR
       COMMON/DCOORD/X,XCOORD
       COMMON/SKI/DRAS,PP
       COMMON/TAB/AA,NF,EPSI,JMAX,DALFA
       COMMON/DATINT/XL,YL
       FONCB=DRAS(1)
C CONSTITUTE TABLES XL AND YL BEFORE INTERPOLATION
       R=SQRT(S*S+PP)
       IF(JMAX.LT.3) THEN
            WRITE(16,*)' JMAX IS TOO SMALL, INTERPOLATION IS',
     +      ' NOT POSSIBLE'
            STOP 871
       ELSE                    !FIND FIRST VALUE OF X(I) LARGER THAN R
            DO J=1,JMAX
            IF(X(J).GE.R) GO TO 1
            END DO
       END IF
 1     IF(J.GE.JMAX-1) J=J-1               !CASE : S.GT.X(JMAX-1)
       DO JF=1,3
           XL(JF)=X(J+JF-2)
           YL(JF)=DRAS(J+JF-2)
       END DO
       CALL SPLINE(XL,YL,AAA,BBB,CCC,DDD,3,IER)
       IF(IER.LT.0) THEN
       WRITE(16,210) IER
210    FORMAT(' ---foncb--> ERROR ',I2,' IN SUBR. SPLINE <-------')
       STOP
       END IF
       CALL ISPLIN(XL,YL,AAA,BBB,CCC,DDD,3,R,FONCB,IER)
       RETURN
       END

C
C     *************************************************************
C     *                                                           *
              SUBROUTINE APP(TH,PCO,P,RES,JVDM,JV,XL,XW,GEXC)
C     *                                                           *
C     *************************************************************
C
C       THIS SUBROUTINE USES A LINEAR INTERPOLATION SCHEME.
C ACCURACY IS PROVIDED BY THE LARGE VALUE OF JMAX AND BY A PROGRESSIVE
C             DECREASE OF THE SPATIAL RESOLUTION.
C
C      LONG SLIT SPECTROSCOPY AND IUE SLITS CASE
C      =========================================
C
       DIMENSION TH(200),P(200),PCO(200),RES(200),BRIGHT(200)
       DIMENSION PL(200),THH(200),PLINE(4000)
       COMMON/TAB/AA,NF,EPSI,JMAX,DALFA
       PI=4.*ATAN(1.)
C Check that a line of sight at least has been selected
       if(jv.eq.0) stop ' No line of sight has been selected'
C Transform tables p and th into tables of LOG(VALUES)
       DO inorm=1,jvdm                  !REDUCE RANGE OF VALUES TO BE
!                                            INTERPLOLATED
       pl(inorm)=alog10(p(inorm))           !PL IN LOG(CENTIMETERS)
       thh(inorm)=alog10(th(inorm))
       END DO
C
C SCAN THE SLIT POSITIONS
       PLIM1=XL*5                        !ALL PLIM'S IN CENTIMETERS
       PLIM2=XL*12
       PLIM3=XL*25
       WRITE(16,*)' SLITCENTER (km)   AVERAGE COL. DENSITY (cm-2)',
     + '    BRIGHTNESS (kR)'
       DO 1 ISLIT=1,JV
       SLITCENTER=PCO(ISLIT)                !SLICENTER IN CENTIMETERS
       NLENGTH=50
       NWIDTH=10
       IF(SLITCENTER.LE.PLIM1) GO TO 5
       NLENGTH=25
       NWIDTH=6
       IF(SLITCENTER.LE.PLIM2) GO TO 5
       NLENGTH=12
       NWIDTH=3
       IF(SLITCENTER.LE.PLIM3) GO TO 5
       NLENGTH=1
       NWIDTH=1
 5     DNW=XW/NWIDTH                          !DNW AND DNL IN CENTIMETERS
       DNL=XL/NLENGTH
C
C STORE VALUES TO BE INTERPOLATED : IT IS ASSUMED THAT THE SLIT HAS ITS
C LONG AXIS (xleng) ORIENTED ALONG THE RADIUS VECTOR.
C
          PMIN=1.E35
          PMAX=0.
          INDEX=0
          DO 2 IW=1,NWIDTH
          DWW=-XW/2.+DNW/2.+(IW-1.)*DNW
          DO 2 IL=1,NLENGTH
          DWL=-XL/2.+DNL/2.+(IL-1.)*DNL
          INDEX=INDEX+1                         !PLINE IN LOG(CENTIMETERS)
          PLIN=SQRT((SLITCENTER+DWL)**2+DWW**2) !PLIN IN CENTIMETERS
          PLINE(INDEX)=ALOG10(PLIN)             !PLINE IN LOG(CM)
C         IF(PLINE(INDEX).LT.PMIN) PMIN=PLINE(INDEX)
C         IF(PLINE(INDEX).GT.PMAX) PMAX=PLINE(INDEX)
 2        CONTINUE
C
C LINEAR INTERPOLATION (LOGARITHMIC SCALES)
          INDEX=0
          RES0=0.
          DO 44 IW=1,NWIDTH
          DO 44 IL=1,NLENGTH
            INDEX=INDEX+1
            PAR=PLINE(INDEX)
            IF(PAR.LE.PL(1)) GOTO 17       !IF PAR.LE.PL(1), TAKE THH(1)
               DO 455 IM=1,JVDM            !FIND FIRST VALUE SMALLER
               IG=IM                       !THAN PAR.
               IF(PL(IM).GE.PAR) GOTO 32
               PINF=PL(IM)
 455           CONTINUE
 32         PSUP=PL(IG)
            DP=(PAR-PINF)/(PSUP-PINF)
            RESULT=THH(IG-1)-(THH(IG-1)-THH(IG))*DP
            GO TO 18
 17         RESULT=THH(1)
 18         RESULT=10**RESULT
            RES0=RES0+RESULT
 44         CONTINUE
       RES(ISLIT)=RES0/INDEX
       BRIGHT(ISLIT)=RES(ISLIT)*GEXC/1.E9    !UNIT IS KILORAYLEIGH
       SLITCENT=SLITCENTER/1.E5            !SLITCENTER IN KILOMETERS
       WRITE(16,220) SLITCENT,RES(ISLIT),BRIGHT(ISLIT)
  1     CONTINUE
 15    format(/)
 210   FORMAT(' ERROR IN SUBROUTINE SPLINE',I3)
 220   FORMAT(4X,E10.3,14X,E10.3,18X,1Pe12.3)
       RETURN
       END

C
C                **********************************
C                *                                *
                         SUBROUTINE VERIF
C                *                                *
C                **********************************
C
C THIS SUBROUTINE COMPUTES THE TOTAL NUMBER OF RADICALS INSIDE THE SPHERE
C OF RADIUS DIM AND COMPARES IT TO THE THEORETICAL VALUE THEORY.
C
      DIMENSION QP(31),QN(31),TOU(31),X(150),DENR(150),XCOORD(150)
      EXTERNAL GAUSS,FONCA
      COMMON/DUR/TAUPT,TAUPD,TAUGT
      COMMON/VIT/VPAR,VDG
      COMMON/TAB/AA,NF,EPSI,JMAX,DALFA
      COMMON/DOR/DENR
      COMMON/SPHER/DIM,RCOMA,TPERM
      COMMON/DCOORD/X,XCOORD
      COMMON/C1/GEXC0,QP,QN,TOU,NSTEPS,RHELIO,RH2,DELTA
      AX = 4.*ATAN(1.)*DIM*DIM*(VDG+VPAR)*DENR(1)
      BW=TAUGT/TAUPD*TAUPT
      THEORY=0.
        DO I=1,NSTEPS
        IF(TOU(I).GT.TPERM) TOU(I)=TPERM*86400.
        T1=TOU(I)/TAUPT
        T2=TOU(I+1)/TAUPT
        EX1=0.
        EX2=0.
        IF(T1.LT.200.) EX1=EXP(-T1)
        IF(T2.LT.200.) EX2=EXP(-T2)
        THEORY=THEORY+QP(I)*(-EX1+EX2)
        END DO
      THEORY=THEORY*BW-AX
      XMIN=X(JMAX)
      DIM1 = XMIN+(DIM-XMIN)/13.
      DIM2 = XMIN+4.*DIM1
      T1 = GAUSS(XMIN,DIM1,8,FONCA,IER)
      T2 = GAUSS(DIM1,DIM2,8,FONCA,IER)
      T3 = GAUSS(DIM2,DIM,8,FONCA,IER)
      TOTAL = T1+T2+T3
      RATIO = TOTAL/THEORY
      WRITE(16,346)TOTAL,THEORY,RATIO
 346  FORMAT(/' NUMBER OF RADICALS IN THE COMA:',E10.3,/,
     +  'THEORETICAL VALUE FOR STEADY STATE IS ',E10.3,
     +  '  TOTAL/THEORY IS ',F5.3)
      RETURN
      END

C
C                *****************************
C                *                           *
                     FUNCTION FONCA(S)
C                *                           *
C                *****************************
C
      DIMENSION DENR(150),X(150),XCOORD(150)
      COMMON/DCOORD/X,XCOORD
      COMMON/DOR/DENR
      COMMON/TAB/AA,NF,EPSI,JMAX,DALFA
C If s is small, the density is assumed to have the first calculated value.
      IF(S.LE.X(JMAX)) GOTO 1
      D=0.
C If s is larger than DIM, the density is assumed to be 0.
      IF(S.GE.X(1)) GOTO 4
      DO 2 I=1,JMAX
         K = I
         IF(X(I).LT.S) GOTO 3
 2    CONTINUE
 3    DX=X(K)-X(K-1)
C Linear interpolation of the density table.
      D=DENR(K-1)+(DENR(K)-DENR(K-1))*(S-X(K-1))/DX
      GOTO 4
 1    D=DENR(JMAX)
 4    FONCA=4.*4.*ATAN(1.)*S*S*D
      RETURN
      END

C
C             **********************************************
C             *                                            *
                       FUNCTION GAUSS(A,B,N,FONC,IER)
C             *                                            *
C             **********************************************
C       A    = LOWER LIMIT
C       B    = UPPER LIMIT
C       N    = NUMBER OF POINTS (8,16,32)
C       FONC = PREVIOUSLY DEFINED FUNCTION TO BE INTEGRATED
C
      EXTERNAL FONC
      DIMENSION X(16),W(16)
      NP=-15863792
      IF (N-NP) 100,40,100
 100  NP=N
      IF(N-8)1,10,1
 1    IF(N-16)2,20,2
 2    IF(N-32)3,30,3
 3    NP=-15863792
      IER=1
      GAUSS=0.E+00
      GO TO 99
C COEFFICIENTS FOR 8 POINTS
 10   CONTINUE
 11   X(1) = .1834346
      X(2) = .5255324
      X(3) = .7966665
      X(4) = .9602898
      W(1) = .3626838
      W(2) = .3137066
      W(3) = .2223810
      W(4) = .1012285
      L = N/2
      GO TO 40
C COEFFICIENTS FOR 16 POINTS
 20   CONTINUE
 21   X(1) = .0950125
      X(2) = .2816035
      X(3) = .4580168
      X(4) = .6178762
      X(5) = .7554044
      X(6) = .8656312
      X(7) = .9445750
      X(8) = .9894009
      W(1) = .1894506
      W(2) = .1826034
      W(3) = .1691565
      W(4) = .1495960
      W(5) = .1246290
      W(6) = .0951585
      W(7) = .0622535
      W(8) = .0271524
      L = N/2
      GO TO 40
C COEFFICIENTS FOR 32 POINTS
 30   CONTINUE
 31   X(1) = .0480
      X(2) = .1444
      X(3) = .2392
      X(4) = .3318
      X(5) = .4213
      X(6) = .5068
      X(7) = .5877
      X(8) = .6630
      X(9) = .7321
      X(10)= .7944
      X(11)= .8493
      X(12)= .8961
      X(13)= .9349
      X(14)= .9647
      X(15)= .9856
      X(16)= .9972
      W(1) = .0965
      W(2) = .0956
      W(3) = .0938
      W(4) = .0911
      W(5) = .0876
      W(6) = .0833
      W(7) = .0781
      W(8) = .0723
      W(9) = .0658
      W(10)= .0586
      W(11)= .0509
      W(12)= .0428
      W(13)= .0342
      W(14)= .0253
      W(15)= .0162
      W(16)= .0070
      L = N/2
      GO TO 40
C REDUCTION TO THE INTERVAL FROM -1 TO +1
 40   APB = (A+B)/2.E+00
      AMB = (B-A)/2.E+00
      S = 0.0
C CALCULATING THE INTEGRAL
      DO 50 I = 1,L
         T1 = APB+AMB*X(I)
         T2 = APB-AMB*X(I)
         Y1 = FONC(T1)
         Y2 = FONC(T2)
 50   S = S+W(I)*(Y1+Y2)
        IER = 0
        GAUSS = S*AMB
 99    RETURN
      END

C
C               ***************************************
C               *                                     *
C               *         SPLINE SUBROUTINE           *
C               *                                     *
C               ***************************************
C  PURPOSE
C       INTERPOLATION WITH NATURAL CUBIC SPLINES
C       EVALUATE COEFFICIENTS OF SPLINES
C
C  USAGE
C       CALL SPLINE(X,Y,A,B,C,D,N,IER)
C
C  DESCRIPTION OF PARAMETERS
C       X       - INPUT ARRAY OF LENGTH N, X-VALUES
C       Y       - INPUT ARRAY OF LENGTH N, Y-VALUES
C       A       - OUTPUT ARRAY OF LENGTH N, COEFFICIENT OF SPLINE
C       B       - SAME AS A
C       C       - SAME AS A
C       D       - SAME AS A
C       N       - NUMBER OF POINTS
C       IER     - ERROR PARAMETER
C               =  0  NO ERROR
C               = -1  TOO LESS DATA POINTS
C               = -2  X(I) NO IN ASCENDING ORDER
C               = -3  TWO IDENTICAL ARGUMENTS
C
C  SUBROUTINES AND FUNCTION SUBPROGRAMS REQUIRED
C       NONE
C
C  REMARKS
C       THE X(I) MUST BE IN ASCENDING ORDER
C
C  METHOD
C       SEE: G. JORDAN-ENGELN, F. REUTTER, NUMERISCHE MATHEMATIK
C            FUER INGENIEURE, B.I., 1972
C
C-
        SUBROUTINE SPLINE(X,Y,A,B,C,D,N,IER)
        DIMENSION X(1),Y(1)
        DOUBLE PRECISION A(1),B(1),C(1),D(1)
        N1=N-1
        IER=0
        IF(N .LT. 3) GO TO 91
C
C  EVALUATE DELTAX AND CHECK THE X(I)
C
        DO 10 I=2,N
        DX=X(I)-X(I-1)
        IF(DX)92,93,2
2       A(I)=1./DX
        C(I-1)=A(I)
10      CONTINUE
C
C  EVALUATE DIAGONAL ELEMENTS OF MATRIX
C
        DO 20 I=2,N1
        B(I)=2.*(A(I)+A(I+1))
20      CONTINUE
        B(1)=2.*A(2)
        B(N)=2.*A(N)
C
C  EVALUATE REST OF EQUATION
C
        DY1=Y(2)-Y(1)
        D(1)=3.*DY1*A(2)**2
        DO 30 I=2,N1
        DY2=Y(I+1)-Y(I)
        D(I)=3.*(DY1*A(I)**2+DY2*A(I+1)**2)
        DY1=DY2
30      CONTINUE
        D(N)=3.*DY2*A(N)**2
C
C  SOLVE EQUATION
C
        DO 40 I=2,N1
        B(I)=C(I-1)*A(I)-B(I)*B(I-1)
        C(I)=-C(I)*B(I-1)
        D(I)=D(I-1)*A(I)-D(I)*B(I-1)
40      CONTINUE
        D(N)=D(N1)*A(N)-D(N)*B(N1)
        B(N)=C(N1)*A(N)-B(N)*B(N1)
C
        B(N)=D(N)/B(N)
        DO 50 I=N1,1,-1
        B(I)=(D(I)-C(I)*B(I+1))/B(I)
50      CONTINUE
C
C  EVALUATE OTHER COEFFICIENTS
C
        DO 60 I=1,N1
        DY1=Y(I+1)-Y(I)
        C(I)=A(I+1)*(3.*DY1*A(I+1)-2*B(I)-B(I+1))
        D(I)=A(I+1)**2 * (-2.*DY1*A(I+1)+B(I)+B(I+1))
60      CONTINUE
        C(1)=0.
        C(N)=0.
        D(N)=0.
        DO 70 I=1,N1
        A(I)=Y(I)
70      CONTINUE
        RETURN
C
C  ERROR CODES
C
91      IER=-1
        RETURN
92      IER=-2
        RETURN
93      IER=-3
        RETURN
C+ ENTRY ISPLIN(XS,YS,IER)
C
C  PURPOSE
C       INTERPOLATE FOR GIVEN ARGUMENT XS
C       INTERPOLATION FUNCTION IS A POLYNAOMIAL OF DEGREE 3
C       FOR EACH INTERVAL [X(I),X(I+1)]
C
C  USAGE
C       CALL ISPLIN(X,Y,A,B,C,D,N,XS,YS,IER)
C
C  DESCRIPTION OF PARAMETERS
C       X       - INPUT ARRAY OF LENGTH N, ARGUMENT VALUES
C       Y       - INPUT ARRAY OF LENGTH N, FUNCTION VALUES
C       A       - ARRAY OF LENGTH N, COEFFICIENT OF POLYNOMIAL
C       B       - SAME AS A
C       C       - SAME AS A
C       D       - SAME AS A
C       N       - INPUT PARAMETER, NUMBER OF DATA
C       XS      - INPUT PARAMETER, ARGUMENT FOR INTERPOLATION
C       YS      - OUTPUT PARAMETER, INTERPOLATED FUNCTION VALUE
C       IER     - ERROR CODE
C               = 0  NO ERROR
C               = 1  LINEAR EXTRAPOLATION DONE
C
C  SUBROUTINES AND FUNCTION SUBPROGRAMS REQUIRED
C       NONE
C
C  REMARKS
C       FOR COMPUTATION OF SPLINE COEFFICIENTS SEE SUBROUTINE SPLINE.
C       EXTRAPOLATION IS DONE LINEAR, FIRST DERIVATIVE IS EQUAL IN
C       X(1) OR X(N)
C-
        ENTRY ISPLIN(X,Y,A,B,C,D,N,XS,YS,IER)
C
C  SEARCH INTERVAL
C
        IER=0
        DX=XS-X(1)
        IF(DX)71,72,73
71      YS=Y(1)+B(1)*DX
        IER=1
        RETURN
72      YS=Y(1)
        RETURN
73      DX=XS-X(N)
        IF(DX)76,75,74
74      YS=Y(N)+B(N)*DX
        IER=1
        RETURN
75      YS=Y(N)
        RETURN
C
C  SEARCH REQUIRED INTERVAL WITH BINARY SEARCH METHOD
C
76      I=1
        J=N
77      K=(J+I)/2
        IF(XS-X(K))78,79,80
78      J=K
        GO TO 81
79      YS=Y(K)
        RETURN
80      I=K
81      IF(IABS(J-I) .GT. 1) GO TO 77
C
C  INTERPOLATION AND EXTRAPOLATION
C
        DX=XS-X(I)
        YS=A(I)+B(I)*DX+C(I)*DX**2+D(I)*DX**3
        RETURN
        END
