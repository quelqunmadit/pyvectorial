#!/usr/bin/env python3

import sys
import copy

import numpy as np
import astropy.units as u
from astropy.visualization import quantity_support
import matplotlib.pyplot as plt
import sbpy.activity as sba

__author__ = 'Shawn Oset, Lauren Lyons'
__version__ = '0.0'

solarbluecol = np.array([38, 139, 220]) / 255.
solarblue = (solarbluecol[0], solarbluecol[1], solarbluecol[2], 1)
solargreencol = np.array([133, 153, 0]) / 255.
solargreen = (solargreencol[0], solargreencol[1], solargreencol[2], 1)
solarblackcol = np.array([0, 43, 54]) / 255.
solarblack = (solarblackcol[0], solarblackcol[1], solarblackcol[2], 1)
solarwhitecol = np.array([238, 232, 213]) / 255.
solarwhite = (solarblackcol[0], solarblackcol[1], solarblackcol[2], 1)


def showRadialPlots(coma, rUnits, volUnits, fragName):
    """ Show the radial density of the fragment species """

    xMin_logplot = np.ceil(np.log10(coma.vModel['CollisionSphereRadius'].to(u.m).value))
    xMax_logplot = np.floor(np.log10(coma.vModel['MaxRadiusOfGrid'].to(u.m).value))

    xMin_linear = 0 * u.m
    xMax_linear = 2000000 * u.m

    linInterpX = np.linspace(xMin_linear.value, xMax_linear.value, num=200)
    linInterpY = coma.vModel['rDensInterpolator'](linInterpX)/(u.m**3)
    linInterpX *= u.m
    linInterpX.to(rUnits)

    logInterpX = np.logspace(xMin_logplot, xMax_logplot, num=200)
    logInterpY = coma.vModel['rDensInterpolator'](logInterpX)/(u.m**3)
    logInterpX *= u.m
    logInterpX.to(rUnits)

    plt.style.use('Solarize_Light2')

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10))

    ax1.set(xlabel=f'Distance from nucleus, {rUnits.to_string()}')
    ax1.set(ylabel=f"Fragment density, {volUnits.unit.to_string()}")
    ax2.set(xlabel=f'Distance from nucleus, {rUnits.to_string()}')
    ax2.set(ylabel=f"Fragment density, {volUnits.unit.to_string()}")
    fig.suptitle(f"Calculated radial density of {fragName}")

    ax1.set_xlim([xMin_linear, xMax_linear])
    ax1.plot(linInterpX, linInterpY, color="red",  linewidth=1.5, linestyle="-", label="cubic spline")
    ax1.plot(coma.vModel['RadialGrid'], coma.vModel['RadialDensity'].to(volUnits), 'bo', label="model")
    ax1.plot(coma.vModel['RadialGrid'], coma.vModel['RadialDensity'].to(volUnits), 'g--', label="linear interpolation")

    ax2.set_xscale('log')
    ax2.set_yscale('log')
    ax2.loglog(coma.vModel['FastRadialGrid'], coma.vModel['RadialDensity'].to(volUnits), 'bo', label="model")
    ax2.loglog(coma.vModel['FastRadialGrid'], coma.vModel['RadialDensity'].to(volUnits), 'g--', label="linear interpolation")
    ax2.loglog(logInterpX, logInterpY, color="red",  linewidth=1.5, linestyle="-", label="cubic spline")

    ax1.set_ylim(bottom=0)
    ax2.set_ylim(bottom=0.1)

    # Mark the beginning of the collision sphere
    ax1.axvline(x=coma.vModel['CollisionSphereRadius'], color=solarblue)
    ax2.axvline(x=coma.vModel['CollisionSphereRadius'], color=solarblue)

    # Mark the collision sphere
    plt.text(coma.vModel['CollisionSphereRadius']*2, linInterpY[0]*2, 'Collision Sphere Edge', color=solarblue)

    plt.legend(loc='upper right', frameon=False)
    plt.show()


def showColumnDensityPlot(coma, rUnits, cdUnits, fragName):
    """ Show the radial density of the fragment species """

    xMin_logplot = np.ceil(np.log10(coma.vModel['CollisionSphereRadius'].to(u.m).value))
    xMax_logplot = np.floor(np.log10(coma.vModel['MaxRadiusOfGrid'].to(u.m).value))

    xMin_linear = 0 * u.m
    xMax_linear = 2000000 * u.m

    linInterpX = np.linspace(xMin_linear.value, xMax_linear.value, num=200)
    linInterpY = coma.vModel['ColumnDensity']['Interpolator'](linInterpX)/(u.m**2)
    linInterpX *= u.m
    linInterpX.to(rUnits)

    logInterpX = np.logspace(xMin_logplot, xMax_logplot, num=200)
    logInterpY = coma.vModel['ColumnDensity']['Interpolator'](logInterpX)/(u.m**2)
    logInterpX *= u.m
    logInterpX.to(rUnits)

    plt.style.use('Solarize_Light2')

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10))

    ax1.set(xlabel=f'Distance from nucleus, {rUnits.to_string()}')
    ax1.set(ylabel=f"Fragment column density, {cdUnits.unit.to_string()}")
    ax2.set(xlabel=f'Distance from nucleus, {rUnits.to_string()}')
    ax2.set(ylabel=f"Fragment column density, {cdUnits.unit.to_string()}")
    fig.suptitle(f"Calculated column density of {fragName}")

    ax1.set_xlim([xMin_linear, xMax_linear])
    ax1.plot(linInterpX, linInterpY, color="red",  linewidth=2.5, linestyle="-", label="cubic spline")
    ax1.plot(coma.vModel['ColumnDensity']['CDGrid'], coma.vModel['ColumnDensity']['Values'], 'bo', label="model")
    ax1.plot(coma.vModel['ColumnDensity']['CDGrid'], coma.vModel['ColumnDensity']['Values'], 'g--', label="linear interpolation")

    ax2.set_xscale('log')
    ax2.set_yscale('log')
    ax2.loglog(coma.vModel['ColumnDensity']['CDGrid'], coma.vModel['ColumnDensity']['Values'], 'bo', label="model")
    ax2.loglog(coma.vModel['ColumnDensity']['CDGrid'], coma.vModel['ColumnDensity']['Values'], 'g--',
               label="linear interpolation", linewidth=0.5)
    ax2.loglog(logInterpX, logInterpY, color="red",  linewidth=0.5, linestyle="-", label="cubic spline")

    ax1.set_ylim(bottom=0)

    ax2.set_xlim(right=coma.vModel['MaxRadiusOfGrid'])

    # Mark the beginning of the collision sphere
    ax1.axvline(x=coma.vModel['CollisionSphereRadius'], color=solarblue)
    ax2.axvline(x=coma.vModel['CollisionSphereRadius'], color=solarblue)

    # Only plot as far as the maximum radius of our grid on log-log plot
    ax2.axvline(x=coma.vModel['MaxRadiusOfGrid'])

    # Mark the collision sphere
    plt.text(coma.vModel['CollisionSphereRadius']*1.1, linInterpY[0]/10, 'Collision Sphere Edge', color=solarblue)

    plt.legend(loc='upper right', frameon=False)
    plt.show()


def showColumnDensity3D(coma, xMin, xMax, yMin, yMax, gridStepX, gridStepY, rUnits, cdUnits, fragName):
    """ 3D plot of column density """

    # mesh grid for units native to the interpolation function
    x = np.linspace(xMin.to(u.m).value, xMax.to(u.m).value, gridStepX)
    y = np.linspace(yMin.to(u.m).value, yMax.to(u.m).value, gridStepY)
    xv, yv = np.meshgrid(x, y)
    z = coma.vModel['ColumnDensity']['Interpolator'](np.sqrt(xv**2 + yv**2))
    # Interpolator spits out m^-2
    fz = (z/u.m**2).to(cdUnits)

    # mesh grid in the units requested for plotting
    xu = np.linspace(xMin.to(rUnits), xMax.to(rUnits), gridStepX)
    yu = np.linspace(yMin.to(rUnits), yMax.to(rUnits), gridStepY)
    xvu, yvu = np.meshgrid(xu, yu)

    plt.style.use('Solarize_Light2')
    plt.style.use('dark_background')
    plt.rcParams['grid.color'] = "black"

    fig = plt.figure(figsize=(20, 20))
    ax = plt.axes(projection='3d')
    # ax.grid(False)
    surf = ax.plot_surface(xvu, yvu, fz, cmap='inferno', vmin=0, edgecolor='none')

    plt.gca().set_zlim(bottom=0)

    ax.set_xlabel(f'Distance, ({rUnits.to_string()})')
    ax.set_ylabel(f'Distance, ({rUnits.to_string()})')
    ax.set_zlabel(f"Column density, {cdUnits.unit.to_string()}")
    plt.title(f"Calculated column density of {fragName}")

    ax.w_xaxis.set_pane_color(solargreen)
    ax.w_yaxis.set_pane_color(solarblue)
    ax.w_zaxis.set_pane_color(solarblack)

    fig.colorbar(surf, shrink=0.5, aspect=5)
    ax.view_init(90, 90)
    plt.show()


def makeInputDict():

    # Fill in parameters to run vectorial model
    HeliocentricDistance = 1.074 * u.AU

    vModelInput = {}

    # # 30 days ago to 15 days ago, production was 1e28,
    # # then jumped to 3e29 from 15 days ago to now
    vModelInput['TimeAtProductions'] = [100] * u.day
    vModelInput['ProductionRates'] = [5.e25]

    # Parent molecule is H2O
    vModelInput['Parent'] = {}
    # vModelInput['Parent']['TotalLifetime'] = 86430 * u.s
    # vModelInput['Parent']['DissociativeLifetime'] = 101730 * u.s
    vModelInput['Parent']['TotalLifetime'] = 20000 * u.s
    vModelInput['Parent']['DissociativeLifetime'] = 20000 * u.s

    # vModelInput['Parent']['DissociativeLifetime'] = sba.photo_timescale('H2O')
    # vModelInput['Parent']['TotalLifetime'] = vModelInput['Parent']['DissociativeLifetime']*0.8

    vModelInput['Parent']['Velocity'] = 0.820 * (u.km/u.s)

    # Fragment molecule is OH
    vModelInput['Fragment'] = {}
    # Cochran and Schleicher, 1993
    vModelInput['Fragment']['Velocity'] = 1.05 * u.km/u.s
    # vModelInput['Fragment']['TotalLifetime'] = sba.photo_timescale('OH') * 0.8
    vModelInput['Fragment']['TotalLifetime'] = 86400 * u.s
    # vModelInput['Fragment']['TotalLifetime'] = 129000 * u.s

    # Adjust some things for heliocentric distance
    rs2 = HeliocentricDistance.value**2
    vModelInput['Parent']['TotalLifetime'] *= rs2
    vModelInput['Parent']['DissociativeLifetime'] *= rs2
    vModelInput['Fragment']['TotalLifetime'] *= rs2
    # Cochran and Schleicher, 1993
    # vModelInput['Parent']['Velocity'] /= np.sqrt(HeliocentricDistance.value)

    # vModelInput['Grid'] = {}
    # vModelInput['Grid']['NumRadialGridpoints'] = 25
    # vModelInput['Grid']['NumAngularGridpoints'] = 40

    vModelInput['PrintDensityProgress'] = True
    return vModelInput


def main():

    quantity_support()

    vModelInputBase = makeInputDict()
    vModelInput = copy.deepcopy(vModelInputBase)

    print("Calculating using sbpy vectorial model ...")
    coma = sba.VectorialModel(0*(1/u.s), 0 * u.m/u.s, vModelInput)

    print("\n\nRadius (km) vs Fragment density (1/cm3)\n---------------------------------------")
    vds = list(zip(coma.vModel['RadialGrid'], coma.vModel['RadialDensity']))
    for pair in vds:
        print(f'{pair[0].to(u.km):7.0f} :\t{pair[1].to(1/(u.cm**3)):5.3e}')

    print("\nRadius (km) vs Column density (1/cm2)\n-------------------------------------")
    cds = list(zip(coma.vModel['ColumnDensity']['CDGrid'], coma.vModel['ColumnDensity']['Values']))
    for pair in cds:
        print(f'{pair[0].to(u.km):7.0f} :\t{pair[1].to(1/(u.cm**2)):5.3e}')

    print(f"Collision radius: {coma.vModel['CollisionSphereRadius']:5.3e}")

    fragTheory = coma.vModel['NumFragmentsTheory']
    fragGrid = coma.vModel['NumFragmentsFromGrid']
    print("\nTheoretical total number of fragments in coma: ", fragTheory)
    print("Total number of fragments from density grid integration: ", fragGrid)
    print(f"Percentage of fragments: {fragGrid/fragTheory}")

    # Set aperture to entire comet to see if we get all of the fragments as an answer
    ap1 = sba.RectangularAperture((coma.vModel['MaxRadiusOfGrid'].value, coma.vModel['MaxRadiusOfGrid'].value) * u.m)
    print("Percent of total fragments recovered by integrating column density over")
    print("\tLarge rectangular aperture: ", coma.total_number(ap1)/fragTheory)

    # One more time
    ap2 = sba.CircularAperture((coma.vModel['MaxRadiusOfGrid'].value) * u.m)
    print("\tLarge circular aperture: ", coma.total_number(ap2)/fragTheory)

    # Try out annular
    ap3 = sba.AnnularAperture([500000000, coma.vModel['MaxRadiusOfGrid'].value]*u.m)
    print("\tAnnular aperture, inner radius 500000 km, outer radius of entire grid:\n\t",
          coma.total_number(ap3)/fragTheory)

    fragName = 'OH'
    showRadialPlots(coma, u.km, 1/u.cm**3, fragName)
    showColumnDensityPlot(coma, u.km, 1/u.cm**3, fragName)
    showColumnDensity3D(coma, -100000*u.km, 10000*u.km, -100000*u.km, 10000*u.km, 1000, 100, u.km, 1/u.cm**2, fragName)
    showColumnDensity3D(coma, -100000*u.km, 100000*u.km, -100000*u.km, 100000*u.km, 1000, 1000, u.km, 1/u.cm**2, fragName)

    # Play with Haser stuff
    # pts =  sba.photo_timescale('CO') * 3 * u.m/u.s
    # print(pts)
    # coma = sba.Haser(5/u.s, 3 * u.m/u.s, pts)
    # ap = sba.RectangularAperture((100, 100)*u.km)
    # print(coma.total_number(ap))
    # print(coma.parent)
    # print(pts)

    # TODO: Start with header identifying the input used for the calculations and dump the densities in a table
    #       along with other results
    # with open('output.yaml', 'w') as outfile:
    #     yaml.dump(inputYAML, outfile)


if __name__ == '__main__':
    sys.exit(main())
