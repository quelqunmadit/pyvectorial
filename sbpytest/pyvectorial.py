#!/usr/bin/env python3

import copy
import numpy
import os
import sys
import yaml

import numpy as np
import astropy.units as u
import matplotlib.pyplot as plt
import sbpy.activity as sba

from argparse import ArgumentParser
# import astropy.io.misc.yaml as apyaml

# from vmodel import VectorialModel

__author__ = 'Shawn Oset, Lauren Lyons'
__version__ = '0.0'

solarbluecol = np.array([38, 139, 220]) / 255.
solarblue = (solarbluecol[0], solarbluecol[1], solarbluecol[2], 1)
solargreencol = np.array([133, 153, 0]) / 255.
solargreen = (solargreencol[0], solargreencol[1], solargreencol[2], 1)
solarblackcol = np.array([0, 43, 54]) / 255.
solarblack = (solarblackcol[0], solarblackcol[1], solarblackcol[2], 1)
solarwhitecol = np.array([238, 232, 213]) / 255.
solarwhite = (solarblackcol[0], solarblackcol[1], solarblackcol[2], 1)


def readParametersYAML(filepath):
    """Read the YAML file with all of the input parameters in it"""
    with open(filepath, 'r') as stream:
        try:
            paramYAMLData = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
    return paramYAMLData


def showRadialPlots(coma, volUnits):
    """ Show the radial density of the fragment species """

    xMin = 0
    # xMax = coma.vModel['MaxRadiusOfGrid'].value
    xMax = 2000000
    fragName = coma.vModelParams['Fragment']['Name']
    # 100km intervals for x axis
    interpArrayX = numpy.linspace(xMin, xMax, 100000)
    # Interpolator spits out m^-3
    interpArrayY = (coma.vModel['rDensInterpolator'](interpArrayX)/(u.m**3)).to(volUnits).value

    plt.style.use('Solarize_Light2')

    plt.figure(figsize=(10, 6), dpi=80)
    plt.xlim(xMin, xMax)
    plt.xlabel('Distance from nucleus (m)')
    plt.ylabel(f"Fragment density, {volUnits.unit.to_string()}")
    plt.title(f"Calculated radial density of {fragName}")

    plt.plot(interpArrayX, interpArrayY, color="red",  linewidth=2.5, linestyle="-", label="cubic spline")
    plt.plot(coma.vModel['FastRadialGrid'], coma.vModel['RadialDensity'].to(volUnits), 'bo', label="model")
    plt.plot(coma.vModel['FastRadialGrid'], coma.vModel['RadialDensity'].to(volUnits), 'g--', label="linear interpolation")
    plt.gca().set_ylim(bottom=0)

    # Mark the beginning of the collision sphere
    plt.axvline(x=coma.vModel['CollisionSphereRadius'].to('m').value)

    plt.legend(loc='upper right', frameon=False)
    plt.show()


def showColumnDensityPlot(coma, cdUnits):
    """ Show the radial density of the fragment species """

    xMin = 0
    xMax = coma.vModel['MaxRadiusOfGrid'].value
    fragName = coma.vModelParams['Fragment']['Name']
    cDensGrid = coma.vModel['ColumnDensity']['FastCDGrid']
    interpArrayX = numpy.linspace(xMin, xMax, 100000)
    interpArrayY = coma.vModel['ColumnDensity']['Interpolator'](interpArrayX)

    plt.style.use('Solarize_Light2')

    plt.figure(figsize=(10, 6), dpi=80)
    plt.xlim(xMin, xMax)
    plt.xlabel('Distance from nucleus (m)')
    plt.ylabel(f"Fragment column density, {cdUnits.unit.to_string()}")
    plt.title(f"Calculated column density of {fragName}")

    plt.plot(interpArrayX, interpArrayY, color="red",  linewidth=2.5, linestyle="-", label="cubic spline")
    plt.plot(cDensGrid, coma.vModel['ColumnDensity']['Values'], 'bo', label="model")
    plt.plot(cDensGrid, coma.vModel['ColumnDensity']['Values'], 'g--', label="linear interpolation")
    plt.gca().set_ylim(bottom=0)

    # Mark the beginning of the collision sphere
    plt.axvline(x=coma.vModel['CollisionSphereRadius'].to('m').value)

    plt.legend(loc='upper right', frameon=False)
    plt.show()


def showColumnDensity3D(coma, xMin, xMax, yMin, yMax, gridStepX, gridStepY, cdUnits):
    """ 3D plot of column density """

    fragName = coma.vModelParams['Fragment']['Name']

    x = numpy.linspace(xMin, xMax, gridStepX)
    y = numpy.linspace(yMin, yMax, gridStepY)
    xv, yv = numpy.meshgrid(x, y)
    z = coma.vModel['ColumnDensity']['Interpolator'](numpy.sqrt(xv**2 + yv**2))
    # Interpolator spits out m^-2
    fz = (z/u.m**2).to(cdUnits)

    plt.style.use('Solarize_Light2')
    plt.style.use('dark_background')
    plt.rcParams['grid.color'] = "black"

    fig = plt.figure(figsize=(20, 20))
    ax = plt.axes(projection='3d')
    # ax.grid(False)
    surf = ax.plot_surface(xv, yv, fz, cmap='inferno', vmin=0, edgecolor='none')

    plt.gca().set_zlim(bottom=0)

    ax.set_xlabel('Distance (m)')
    ax.set_ylabel('Distance (m)')
    ax.set_zlabel(f"Column density, {cdUnits.unit.to_string()}")
    plt.title(f"Calculated column density of {fragName}")

    ax.w_xaxis.set_pane_color(solargreen)
    ax.w_yaxis.set_pane_color(solarblue)
    ax.w_zaxis.set_pane_color(solarblack)

    fig.colorbar(surf, shrink=0.5, aspect=5)
    ax.view_init(90, 90)
    plt.show()


def main():

    # Parse command-line arguments
    parser = ArgumentParser(
        usage='%(prog)s [options] [inputfile]',
        description=__doc__,
        prog=os.path.basename(sys.argv[0])
    )
    parser.add_argument('--version', action='version', version=__version__)
    parser.add_argument('--verbose', '-v', action='count', default=0,
                        help='increase verbosity level')
    parser.add_argument(
            'parameterfile', nargs=1, help='YAML file with observation and molecule data in it'
            )  # the nargs=? specifies 0 or 1 arguments: it is optional
    args = parser.parse_args()

    print(f'Loading input from {args.parameterfile[0]} ...')
    # Read in our stuff
    inputYAML = readParametersYAML(args.parameterfile[0])

    # Check if the length ProductionRates TimeAtProductions is the same, otherwise we're in trouble
    if(len(inputYAML['Comet']['ProductionRates']) != len(inputYAML['Comet']['TimeAtProductions'])):
        print("Mismatched lengths for ProductionRates and TimeAtProductions!  Exiting.")
        return 1

    cometData = copy.deepcopy(inputYAML)

    # apply units
    cometData['Comet']['HeliocentricDistance'] *= u.AU
    cometData['Comet']['TimeAtProductions'] *= u.day

    cometData['Parent']['TotalLifetime'] *= u.s
    cometData['Parent']['DissociativeLifetime'] *= u.s
    # cometData['Parent']['DissociativeLifetime'] = sba.photo_timescale('H2O')
    cometData['Parent']['Sigma'] *= u.cm**2

    cometData['Fragment']['Velocity'] *= u.km/u.s
    cometData['Fragment']['TotalLifetime'] *= u.s

    # Adjust some things for heliocentric distance
    rs = cometData['Comet']['HeliocentricDistance'].to(u.AU).value
    cometData['Parent']['TotalLifetime'] *= rs**2
    cometData['Parent']['DissociativeLifetime'] *= rs**2
    cometData['Fragment']['TotalLifetime'] *= rs**2

    v = 0.85 * u.km/u.s

    print("Calculating using vectorial model ...")
    coma = sba.VectorialModel(0*(1/u.s), v.to(u.m/u.s), cometData)

    print("\n\nRadius (km) vs Fragment density (1/cm3)\n---------------------------------------")
    # Print in cm^-3 to compare with fortran output
    for i in range(0, coma.vModel['NumRadialGridpoints']):
        print(f"{coma.vModel['RadialGrid'][i]:10.1f} : {coma.vModel['RadialDensity'][i].to(1/(u.cm**3)):8.4f}")

    print("\nRadius (km) vs Column density (1/cm2)\n-------------------------------------")
    cds = list(zip(coma.vModel['ColumnDensity']['CDGrid'], coma.vModel['ColumnDensity']['Values']))
    for pair in cds:
        print(f'{pair[0]:7.0f} :\t{pair[1].to(1/(u.cm*u.cm)):5.3e}')

    fragTheory = coma.vModel['NumFragmentsTheory']
    fragGrid = coma.vModel['NumFragmentsFromGrid']
    print("Theoretical total number of fragments in coma: ", fragTheory)
    print("Total number of fragments from density grid integration: ", fragGrid)
    print(f"Percentage of fragments: {fragGrid/fragTheory}")

    # # Set aperture to entire comet to see if we get all of the fragments as an answer
    # ap1 = sba.RectangularAperture((coma.vModel['MaxRadiusOfGrid'].value, coma.vModel['MaxRadiusOfGrid'].value) * u.m)
    # print("Percent of total fragments recovered by integrating column density over")
    # print("\tLarge rectangular aperture: ", coma.total_number(ap1)/fragTheory)

    # # One more time
    # ap2 = sba.CircularAperture((coma.vModel['MaxRadiusOfGrid'].value) * u.m)
    # print("\tLarge circular aperture: ", coma.total_number(ap2)/fragTheory)

    # # Try out annular
    # ap3 = sba.AnnularAperture([500000000, coma.vModel['MaxRadiusOfGrid'].value]*u.m)
    # print("\tAnnular aperture, inner radius 500000 km, outer radius of entire grid:\n\t",
    #       coma.total_number(ap3)/fragTheory)

    showRadialPlots(coma, 1/u.cm**3)
    showColumnDensityPlot(coma, 1/u.cm**3)
    showColumnDensity3D(coma, -100000000, 10000000, -100000000, 10000000, 1000, 100, 1/u.cm**2)
    showColumnDensity3D(coma, -100000000, 100000000, -100000000, 100000000, 1000, 1000, 1/u.cm**2)

    # print(coma.vModel)

    # Play with Haser stuff
    # pts =  sba.photo_timescale('CO') * 3 * u.m/u.s
    # print(pts)
    # coma = sba.Haser(5/u.s, 3 * u.m/u.s, pts)
    # ap = sba.RectangularAperture((100, 100)*u.km)
    # print(coma.total_number(ap))
    # print(coma.parent)
    # print(pts)

    # TODO: Start with header identifying the input used for the calculations and dump the densities in a table
    #       along with other results
    # with open('output.yaml', 'w') as outfile:
    #     yaml.dump(inputYAML, outfile)


if __name__ == '__main__':
    sys.exit(main())
